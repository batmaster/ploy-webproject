<?php
    require 'db_config.php';

    $c_id = $_COOKIE["id"];
    $c_name = $_COOKIE["name"];
    $c_email = $_COOKIE["email"];
    $c_type = $_COOKIE["type"];

    if ($_POST["item-transaction-borrow-approve"]) {
        if ($_POST["item-transaction-borrow-approve"] == "ajax_get_available_item") {
            $item_transaction_id = $_POST["id"];
            $sql = "SELECT it.item_type_id FROM item_transaction it WHERE it.id = $item_transaction_id";

            $result = mysql_query($sql) or die(mysql_error());
            while($r = mysql_fetch_assoc($result)) {
                $item_type_id = $r["item_type_id"];
            }

            $sql = "SELECT i.id, i.sn FROM item i WHERE i.status = 0 AND i.item_type_id = $item_type_id
            AND (SELECT it.type FROM item_transaction it, item_transaction_detail itd
                WHERE itd.item_id = i.id
                AND itd.item_transaction_id = it.id ORDER BY it.id DESC LIMIT 1) NOT IN (1, 2)";

            $result = mysql_query($sql) or die(mysql_error());
            $rows = array();
            while($r = mysql_fetch_assoc($result)) {
                $rows[] = $r;
            }
            echo json_encode($rows);
        }
    }
    else if ($_POST["item-transaction-return-approve"]) {
        if ($_POST["item-transaction-return-approve"] == "ajax_get_available_item") {
            $item_transaction_id = $_POST["id"];
            $sql = "SELECT it.item_type_id, it.applicant_id FROM item_transaction it WHERE it.id = $item_transaction_id";

            $result = mysql_query($sql) or die(mysql_error());
            while($r = mysql_fetch_assoc($result)) {
                $item_type_id = $r["item_type_id"];
                $borrower_id = $r["applicant_id"];
            }

            $sql = "SELECT i.id, i.sn FROM item i WHERE i.status = 0 AND i.item_type_id = $item_type_id
            AND i.id IN (SELECT itd2.item_id FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.type = 2 AND it2.applicant_id = $borrower_id AND itd2.item_transaction_id = it2.id)";

            $result = mysql_query($sql) or die(mysql_error());
            $rows = array();
            while($r = mysql_fetch_assoc($result)) {
                $rows[] = $r;
            }
            echo json_encode($rows);
        }
    }
    else if ($_POST["item-edit-remove"]) {
        if ($_POST["item-edit-remove"] == "ajax_get_available_item") {
            $item_transaction_id = $_POST["id"];
            $sql = "SELECT it.item_type_id, it.applicant_id FROM item_transaction it WHERE it.id = $item_transaction_id";
            $result = mysql_query($sql) or die(mysql_error());
            while($r = mysql_fetch_assoc($result)) {
                $item_type_id = $r["item_type_id"];
            }

            $sql = "SELECT i.id, i.sn FROM item i WHERE i.item_type_id = $item_type_id AND i.status != 0";

            $result = mysql_query($sql) or die(mysql_error());
            $rows = array();
            while($r = mysql_fetch_assoc($result)) {
                $rows[] = $r;
            }
            echo json_encode($rows);
        }
    }
?>
