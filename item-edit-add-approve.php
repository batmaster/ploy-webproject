<?php
    if (!(isset($c_type) && ($c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
if (isset($_POST["add"])) {
    $id = $_POST["id"];
    $sn = $_POST["sn"];

    if (!isset($id)) {
        echo "
        <div class='alert alert-danger alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            กรุณาเลือกเลขที่ใบขอซื้อ
        </div>
        ";
    }
    else {
        $sql = "SELECT it.item_type_id, ity.type FROM item_transaction it, item_type ity WHERE it.id = $id AND ity.id = it.item_type_id";
        $result = mysql_query($sql) or die(mysql_error());
        while ($r = mysql_fetch_assoc($result)) {
            $item_type_id = $r["item_type_id"];
            $type = $r["type"];
        }

        $sql = "INSERT INTO item (item_type_id, sn, status, date) VALUES ($item_type_id, '$sn', 0, NOW())";
        mysql_query($sql) or die(mysql_error());

        $sql = "SELECT id FROM item ORDER BY id DESC LIMIT 1";
        $result = mysql_query($sql) or die(mysql_error());
        while ($r = mysql_fetch_assoc($result)) {
            $item_id = $r["id"];
        }

        $sql = "INSERT INTO item_transaction_detail (approver_id, item_transaction_id, item_id, date) VALUES ($c_id, $id, $item_id, NOW())";
        mysql_query($sql) or die(mysql_error());

        echo "
        <div class='alert alert-success alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            ขอเพิ่มอุปกรณ์ประเภท $type เลขทะเบียนอุปกรณ์ $sn เรียบร้อยแล้ว
        </div>
        ";
    }
}

?>

<div class="panel panel-default">
    <div class="panel-heading">อนุมัติการเพิ่มอุปกรณ์</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-offset-3 col-xs-6">
                <form method="POST" action="?page=item-edit-add-approve">
                    <input type="hidden" name="add">

                    <div class="input-group">
                        <span class="input-group-addon">เลขที่ใบสั่งซื้อ</span>
                        <div class="form-group">
                            <select class="form-control" name="id" required>
                                <option value="-1" disabled selected>กรุณาเลือก</option>
                                <?php
                                    $sql = "SELECT it.id, ity.type FROM item_transaction it, item_type ity WHERE it.item_type_id = ity.id AND it.amount > (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id) AND it.type = 0 ORDER BY it.id DESC";

                                    $result = mysql_query($sql);
                                    while ($r = mysql_fetch_assoc($result)) {
                                        $id = $r["id"];
                                        $type = $r["type"];

                                        echo "<option value='$id'>$id $type</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">เลขทะเบียนอุปกรณ์</span>
                        <input type="text" name="sn" class="form-control" required>
                    </div><br>


                    <center>
                        <input type="submit" class="btn btn-primary" value="เพิ่ม">
                        <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">เลขที่ใบสั่งซื้อที่ยังอนุมัติไม่ครบ</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-edit-add-approve">
            <div class="input-group">
                <input type="hidden" name="page" value="item-edit-add-approve">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขที่ใบสั่งซื้อ</th>
                    <th>ชื่อผู้เพิ่ม</th>
                    <th>ประเภทอุปกรณ์</th>
                    <th>จำนวนขอเพิ่ม</th>
                    <th>จำนวนอนุมัติ</th>
                    <th>วันที่ขอเพิ่ม</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT it.id,
                u.name applicant_name,
                ity.type,
                it.amount,
                (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id) amount_aprove,
                it.date
                FROM item_transaction it, item_type ity, user u
                WHERE it.item_type_id = ity.id
                AND it.type = 0
                AND it.applicant_id = u.id
                AND it.amount > (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id)
                AND (it.id LIKE '%$search%' OR u.name LIKE '%$search%' OR ity.type LIKE '%$search%' OR it.date LIKE '%$search%')
                ORDER BY it.id DESC";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $applicant_name = $r["applicant_name"];
                    $type = $r["type"];
                    $amount = $r["amount"];
                    $amount_aprove = $r["amount_aprove"];
                    $date = $r["date"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>
                                <form method='POST' action='?page=item-edit-add-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <input type='hidden' name='forpage' value='item-edit-add-detail'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$id</a>
                                </form>
                            </td>
                            <td>$applicant_name</td>
                            <td>$type</td>
                            <td>$amount</td>
                            <td>$amount_aprove</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='8'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของชื่อผู้ทำรายการ ประเภทอุปกรณ์<br>หรือเลขที่ใบสั่งซื้อ<br>หรือบางส่วนของวันที่สมัครในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>
