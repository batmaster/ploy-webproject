
<?php
    if ($c_type == 0 || $c_type == 1) {
        $active = in_array($_GET['page'], array('user-register')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=user-register'>สมัครสมาชิก</a></li>";

        $active = (in_array($_GET['page'], array('user-approve')) || in_array($_POST['forpage'], array('user-approve'))) && $_POST['id'] != $c_id ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=user-approve'>อนุมัติการสมัครสมาชิก</a></li>";

        $active = (in_array($_GET['page'], array('user-staff')) || in_array($_POST['forpage'], array('user-staff'))) && $_POST['id'] != $c_id ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=user-staff'>ดูข้อมูลเจ้าหน้าที่</a></li>";

        $active = (in_array($_GET['page'], array('user-student')) || in_array($_POST['forpage'], array('user-student'))) && $_POST['id'] != $c_id ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=user-student'>ดูข้อมูลผู้ใช้ทั่วไป</a></li>";

        $active = in_array($_GET['page'], array('user-detail')) && ($_POST['id'] == $c_id || !isset($_POST['id'])) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=user-detail'>ดูข้อมูลตนเอง</a></li>";
    }
    else if ($c_type == 2) {
        $active = in_array($_GET['page'], array('user-register', 'user-approve', 'user-staff', 'user-student', 'user-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=user-detail'>ข้อมูลสมาชิก</a></li>";
    }
?>
