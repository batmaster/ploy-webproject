<?php

if (isset($_POST["update"])) {
    $id = $_POST["id"];
    $name = $_POST["name"] == "" ? "name = name" : "name = '" .$_POST["name"] ."'";
    $phone = $_POST["phone"] == "" ? "phone = phone" : "phone = '" .$_POST["phone"] ."'";
    $type = $c_type != 0 ? "type = type" : "type = " .$_POST["type"];
    $email = $_POST["email"] == "" ? "email = email" : "email = '" .$_POST["email"] ."'";
    $password = $_POST["password"] == "" ? "password = password" : "password = '" .$_POST["password"] ."'";


    $sql = "UPDATE user SET $name, $phone, $type, $email, $password WHERE id = $id";
    mysql_query($sql) or die(mysql_error());
    echo "
    <div class='alert alert-success alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        แก้ไขผู้ใช้แล้ว
    </div>
    ";

    if ($id == $c_id) {
        echo "
            <script type='text/javascript'>
                alert('ออกจากระบบอัตโนมัติ');
                $('#form-logout').submit();
            </script>
        ";
    }
}

if (!isset($_POST["id"])) {
    echo "
    <div class='alert alert-info alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        การแก้ไขข้อมูลตนเองจะทำให้ถูก <strong>ออกจากระบบ</strong> โดยอัตโนมัติ
    </div>
    ";
}


$id = isset($_POST["id"]) ? $_POST["id"] : $c_id;

$sql = "SELECT * FROM user WHERE id = $id";
$result = mysql_query($sql);

$valid = true;
while ($r = mysql_fetch_assoc($result)) {
    $name = $r["name"];
    $phone = $r["phone"];
    $email = $r["email"];
    $type = $r["type"];
    $date = $r["date"];
}


?>
<div class="panel panel-default">
    <div class="panel-heading">ข้อมูลผู้ใช้</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-offset-3 col-xs-6">
                <form method="POST" action="?page=user-detail">
                    <input type="hidden" name="update">
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <div class="input-group">
                        <span class="input-group-addon">ชื่อผู้ใช้</span>
                        <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" required>
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon">เบอร์โทร</span>
                        <input type="text" name="phone" maxlength="10" class="form-control" pattern="[0-9]{10}" value="<?php echo $phone; ?>" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'หมายเลขโทรศัพท์ 10 หลัก' : '');" required>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">ตำแหน่ง</span>
                        <div class="form-group">
                            <select class="form-control" name="type" <?php echo $c_type != 0 ? "disabled" : "" ?> required>
                                <option value="0" <?php echo $type == 0 ? "selected" : "" ?>>ผู้อำนวยการ</option>
                                <option value="1" <?php echo $type == 1 ? "selected" : "" ?>>เจ้าหน้าที่</option>
                                <option value="2" <?php echo $type == 2 ? "selected" : "" ?>>นักเรียน</option>
                            </select>
                        </div>
                    </div><br>

                    <div class="input-group" >
                        <span class="input-group-addon">สมัครเมื่อวันที่</span>
                        <input type="text" disabled class="form-control" value="<?php echo $date; ?>">
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">อีเมล์</span>
                        <input type="email" name="email" class="form-control" value="<?php echo $email; ?>" required>
                    </div><br>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="checkbox"> เปลี่ยนรหัสผ่าน
                        </label>
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon">รหัสผ่าน</span>
                        <input type="text" name="password" id="password" disabled class="form-control" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'อย่างน้อย 6 ตัวอักษร' : ''); if(this.checkValidity()) form.password2.pattern = this.value;" required>
                    </div><br>
                    <div class="input-group" >
                        <span class="input-group-addon">ยืนยันรหัสผ่าน</span>
                        <input type="password" name="password2" id="password2" disabled class="form-control" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'ยืนยันรหัสไม่ถูกต้อง' : '');" required>
                    </div><br>

                    <center>
                        <input type="submit" class="btn btn-primary" value="แก้ไข">
                        <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#checkbox").change(function () {
    if ($(this).is(':checked')) {
        $("#password").removeAttr('disabled');
        $("#password2").removeAttr('disabled');
    } else {
        $("#password").val("");
        $("#password2").val("");
        $("#password").attr('disabled','disabled');
        $("#password2").attr('disabled','disabled');
    }
});
</script>
