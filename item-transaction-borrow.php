<?php
    if (!(isset($c_type) && ($c_type == 2))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php

if (isset($_POST["add"])) {
    $type = $_POST["type"];
    $amount = $_POST["amount"];

    $sql = "INSERT INTO item_transaction (applicant_id, item_type_id, amount, type, date) VALUES ($c_id, $type, $amount, 2, NOW())";
    $result = mysql_query($sql);


    $sql = "SELECT type FROM item_type WHERE id = $type";

    $result = mysql_query($sql);
    while ($r = mysql_fetch_assoc($result)) {
        $type = $r["type"];
    }

    echo "
    <div class='alert alert-success alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        ขอยืมอุปกรณ์ประเภท $type จำนวน $amount เรียบร้อยแล้ว<br>โปรดรออนุมัติทะเบียนอุปกรณ์
    </div>
    ";
}

?>

<div class="panel panel-default">
    <div class="panel-heading">ยืมอุปกรณ์</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-offset-3 col-xs-6">
                <form method="POST" action="?page=item-transaction-borrow">
                    <input type="hidden" name="add">

                    <div class="input-group">
                        <span class="input-group-addon">ประเภทอุปกรณ์</span>
                        <div class="form-group">
                            <select class="form-control" name="type" required>
                                <?php
                                    $sql = "SELECT * FROM item_type it";

                                    $result = mysql_query($sql);
                                    while ($r = mysql_fetch_assoc($result)) {
                                        $id = $r["id"];
                                        $type = $r["type"];

                                        echo "<option value='$id'>$type</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">จำนวนขอยืม</span>
                        <input type="number" name="amount" class="form-control" required>
                    </div><br>


                    <center>
                        <input type="submit" class="btn btn-primary" value="ยืม">
                        <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">รายการยืมอุปกรณ์</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-transaction-borrow">
            <div class="input-group">
                <input type="hidden" name="page" value="item-transaction-borrow">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขที่ใบขอยืม</th>
                    <th>ชื่อผู้ทำรายการ</th>
                    <th>ประเภทอุปกรณ์</th>
                    <th>จำนวนขอยืม</th>
                    <th>จำนวนอนุมัติ</th>
                    <th>วันที่ขอทำรายการ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT it.id,
                u.name applicant_name,
                ity.type,
                it.amount,
                (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id) amount_aprove,
                it.date
                FROM item_transaction it, item_type ity, user u
                WHERE it.item_type_id = ity.id
                AND it.type = 2
                AND it.applicant_id = u.id
                AND (it.id LIKE '%$search%' OR u.name LIKE '%$search%' OR ity.type LIKE '%$search%' OR it.date LIKE '%$search%')
                AND it.applicant_id = $c_id
                ORDER BY it.id DESC";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $applicant_name = $r["applicant_name"];
                    $type = $r["type"];
                    $amount = $r["amount"];
                    $amount_aprove = $r["amount_aprove"];
                    $date = $r["date"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>
                                <form method='POST' action='?page=item-transaction-borrow-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <input type='hidden' name='forpage' value='item-edit-add-detail'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$id</a>
                                </form>
                            </td>
                            <td>$applicant_name</td>
                            <td>$type</td>
                            <td>$amount</td>
                            <td>$amount_aprove</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='8'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของชื่อผู้ทำรายการ ประเภทอุปกรณ์<br>หรือเลขที่ใบสั่งซื้อ<br>หรือบางส่วนของวันที่สมัครในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>
