<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
if (isset($_POST["approve"])) {
    $id = $_POST["id"];
    $email = $_POST["email"];
    $status = $_POST["status"] == 1 ? 0 : 1;

    $class = $status == 1 ? "success" : "danger";
    $operation = $status == 1 ? "เปิด" : "ปิด";

    $sql = "UPDATE user SET status = $status WHERE id = $id";
    $result = mysql_query($sql);

    echo "
    <div class='alert alert-$class alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        $operation การใช้งาน $email เรียบร้อยแล้ว
    </div>
    ";
}

?>

<div class="panel panel-default">
    <div class="panel-heading">ข้อมูลผู้ใช้ทั่วไป</div>
    <div class="panel-body">
        <form method="GET" action="?page=user-student">
            <div class="input-group">
                <input type="hidden" name="page" value="user-student">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อผู้ใช้</th>
                    <th>เบอร์โทร</th>
                    <th>อีเมล์</th>
                    <th>วันที่สมัคร</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT * FROM user WHERE type = 2 AND (name LIKE '%$search%' OR phone LIKE '%$search%' OR email LIKE '%$search%' OR date LIKE '%$search%') ORDER BY id DESC";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $name = $r["name"];
                    $phone = $r["phone"];
                    $email = $r["email"];
                    $date = $r["date"];
                    $status = $r["status"];

                    $operation = $status == 0 ? "เปิดการใช้งาน" : "ปิดการใช้งาน";
                    $class = $status == 0 ? "success" : "danger";
                    $glyphicon = $status == 0 ? "ok" : "remove";

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>$name</td>
                            <td>$phone</td>
                            <td>
                                <form method='POST' action='?page=user-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <input type='hidden' name='forpage' value='user-student'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$email</a>
                                </form>
                            </td>
                            <td>$date</td>
                            <td title='$operation $email'>
                                <form method='POST' action='?page=user-student'>
                                <input type='hidden' name='approve'>
                                <input type='hidden' name='id' value='$id'>
                                <input type='hidden' name='email' value='$email'>
                                <input type='hidden' name='status' value='$status'>
                                <button type='submit' class='btn btn-$class'><span class='glyphicon glyphicon-$glyphicon' aria-hidden='true'></span></button>
                                </form>
                            </td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='7'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของชื่อผู้ใช้ เบอร์โทร อีเมล<br>หรือบางส่วนของวันที่สมัครในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>
