<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<div class="panel panel-default">
    <div class="panel-heading">รายการคืนอุปกรณ์</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-transaction-borrow">
            <div class="input-group">
                <input type="hidden" name="page" value="item-transaction-return-report">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขที่ใบขอคืน</th>
                    <th>ชื่อผู้ทำรายการ</th>
                    <th>ประเภทอุปกรณ์</th>
                    <th>จำนวนขอคืน</th>
                    <th>จำนวนอนุมัติ</th>
                    <th>วันที่ขอทำรายการ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT it.id,
                u.name applicant_name,
                ity.type,
                it.amount,
                (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id) amount_aprove,
                it.date
                FROM item_transaction it, item_type ity, user u
                WHERE it.item_type_id = ity.id
                AND it.type = 3
                AND it.applicant_id = u.id
                AND (it.id LIKE '%$search%' OR u.name LIKE '%$search%' OR ity.type LIKE '%$search%' OR it.date LIKE '%$search%')
                ORDER BY it.id DESC";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $applicant_name = $r["applicant_name"];
                    $type = $r["type"];
                    $amount = $r["amount"];
                    $amount_aprove = $r["amount_aprove"];
                    $date = $r["date"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>
                                <form method='POST' action='?page=item-transaction-return-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$id</a>
                                </form>
                            </td>
                            <td>$applicant_name</td>
                            <td>$type</td>
                            <td>$amount</td>
                            <td>$amount_aprove</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='8'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของชื่อผู้คืน ประเภทอุปกรณ์<br>หรือเลขที่ใบสั่งซื้อ<br>หรือบางส่วนของวันที่สมัครในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>
