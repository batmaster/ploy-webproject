
<?php
    if ($c_type == 0) {
        $active = in_array($_GET['page'], array('item-transaction-borrow-report', 'item-transaction-borrow-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-borrow-report'>ดูรายงานการยืมอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-transaction-return-report', 'item-transaction-return-report')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-return-report'>ดูรายงานการอนุมัติอุปกรณ์</a></li>";
    }
    else if ($c_type == 1) {
        $active = in_array($_GET['page'], array('item-transaction-borrow-approve')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-borrow-approve'>อนุมัติการยืมอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-transaction-borrow-report', 'item-transaction-borrow-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-borrow-report'>ดูรายงานการยืมอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-transaction-return-approve')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-return-approve'>อนุมัติการคืนอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-transaction-return-report', 'item-transaction-return-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-return-report'>ดูรายงานการคืนอุปกรณ์</a></li>";
    }
    else if ($c_type == 2) {
        $active = in_array($_GET['page'], array('item-transaction-borrow', 'item-transaction-borrow-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-borrow'>การยืมอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-transaction-return')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-transaction-return'>การคืนอุปกรณ์</a></li>";

    }
?>
