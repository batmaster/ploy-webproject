<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
if (isset($_POST["add-type"])) {
    $type = $_POST["type"];

    $sql = "INSERT INTO item_type (type) VALUES ('$type')";
    $result = mysql_query($sql);

    echo "
    <div class='alert alert-success alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        เพิ่มประเภทของอุปกรณ์ $type เรียบร้อยแล้ว
    </div>
    ";
}

?>

<?php
    if ($c_type == 1) {
        echo "
        <div class='panel panel-default'>
            <div class='panel-heading'>เพิ่มประเภทของอุปกรณ์</div>
            <div class='panel-body'>
                <div class='row'>
                    <div class='col-xs-offset-3 col-xs-6'>
                        <form method='POST' action='?page=item-type'>
                            <input type='hidden' name='add-type'>
                            <div class='input-group'>
                                <span class='input-group-addon'>ประเภทของอุปกรณ์</span>
                                <input type='text' name='type' class='form-control' required>
                            </div><br>


                            <center>
                                <input type='submit' class='btn btn-primary' value='เพิ่มประเภท'>
                                <input type='dismiss' class='btn btn-danger' value='ยกเลิก'>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        ";
    }
?>

<div class="panel panel-default">
    <div class="panel-heading">ข้อมูลประเภทของอุปกรณ์</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-type">
            <div class="input-group">
                <input type="hidden" name="page" value="item-type">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ประเภทของอุปกรณ์</th>
                    <th>จำนวนทั้งหมด</th>
                    <th>จำนวนคงคลัง</th>
                    <th>จำนวนที่ถูกยืม</th>
                    <th>จำนวนที่ใช้การไม่ได้</th>
                    <th>จำนวนที่สูญหาย</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT ity.id, ity.type, (SELECT COUNT(*) FROM item i WHERE i.item_type_id = ity.id) amount_total,

                (SELECT COUNT(*) FROM item i WHERE i.item_type_id = ity.id) - (
                    (SELECT COUNT(*) FROM item_transaction_detail itd, item i, item_transaction it WHERE itd.item_id = i.id AND i.item_type_id = ity.id AND itd.item_transaction_id = it.id AND it.type = 2) -
                    (SELECT COUNT(*) FROM item_transaction_detail itd, item i, item_transaction it WHERE itd.item_id = i.id AND i.item_type_id = ity.id AND itd.item_transaction_id = it.id AND it.type = 3)
                ) amount_available,

                (
                    (SELECT COUNT(*) FROM item_transaction_detail itd, item i, item_transaction it WHERE itd.item_id = i.id AND i.item_type_id = ity.id AND itd.item_transaction_id = it.id AND it.type = 2) -
                    (SELECT COUNT(*) FROM item_transaction_detail itd, item i, item_transaction it WHERE itd.item_id = i.id AND i.item_type_id = ity.id AND itd.item_transaction_id = it.id AND it.type = 3)
                )amount_borrowed,

                (SELECT COUNT(*) FROM item i WHERE i.item_type_id = ity.id AND i.status = 1) amount_broken,
                (SELECT COUNT(*) FROM item i WHERE i.item_type_id = ity.id AND i.status = 2) amount_lost
                FROM item_type ity
                WHERE ity.type LIKE '%$search%'
                ";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $type = $r["type"];
                    $amount_total = $r["amount_total"];
                    $amount_available = $r["amount_available"];
                    $amount_borrowed = $r["amount_borrowed"];
                    $amount_broken = $r["amount_broken"];
                    $amount_lost = $r["amount_lost"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>$type</td>
                            <td>$amount_total</td>
                            <td>$amount_available</td>
                            <td>$amount_borrowed</td>
                            <td>$amount_broken</td>
                            <td>$amount_broken</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='7'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของประเภทอุปกรณ์</p>
    </div>
</div>
