
<?php
    if ($c_type == 0) {
        $active = in_array($_GET['page'], array('item-edit-add', 'item-edit-add-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-edit-add'>เพิ่มอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-edit-remove', 'item-edit-remove-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-edit-remove'>จำหน่ายอุปกรณ์</a></li>";
    }
    else if ($c_type == 1) {
        $active = in_array($_GET['page'], array('item-edit-add', 'item-edit-add-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-edit-add'>เพิ่มอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-edit-add-approve')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-edit-add-approve'>อนุมัติการเพิ่มอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-edit-remove', 'item-edit-remove-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-edit-remove'>จำหน่ายอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-edit-remove-approve')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-edit-remove-approve'>อนุมัติการจำหน่ายอุปกรณ์</a></li>";
    }
?>
