<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
    $item_id = $_POST["id"];

    $sql = "SELECT i.sn, ity.id item_type_id, i.status FROM item i, item_type ity WHERE i.item_type_id = ity.id AND i.id = $item_id";

    $result = mysql_query($sql);
    while ($r = mysql_fetch_assoc($result)) {
        $sn = $r["sn"];
        $item_type_id = $r["item_type_id"];
        $status = $r["status"];
    }

    if (isset($_POST["edit"])) {
        $sn = $_POST["sn"];
        $type = $_POST["type"];
        $status = $_POST["status"];

        $sql = "UPDATE item SET sn ='$sn', item_type_id ='$type', status = '$status' WHERE id = $item_id";
        mysql_query($sql) or die();
    }
?>


<div class="panel panel-default">
    <div class="panel-heading">แก้ไขข้อมูลอุปกรณ์</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-offset-3 col-xs-6">
                <form method="POST" action="?page=item-detail">
                    <input type="hidden" name="edit">
                    <input type="hidden" name="id" value="<?php echo $item_id;?>">

                    <div class="input-group">
                        <span class="input-group-addon">ทะเบียนอุปกรณ์</span>
                        <input type="text" name="sn" class="form-control" value="<?php echo $sn; ?>" required>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">ประเภทอุปกรณ์</span>
                        <div class="form-group">
                            <select class="form-control" name="type" required>
                                <?php
                                    $sql = "SELECT * FROM item_type it";

                                    $result = mysql_query($sql);
                                    while ($r = mysql_fetch_assoc($result)) {
                                        $id = $r["id"];
                                        $type = $r["type"];
                                        $selected = ($id == $item_type_id ? "selected" : "");

                                        echo "<option value='$id' $selected>$type</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div><br>

                    <p>สถานะการใช้งาน<p>
                    <div class="radio">
                      <label><input type="radio" name="status" value="0" <?php echo $status == 0 ? "checked" : ""; ?>>ปกติ</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="status" value="1" <?php echo $status == 1 ? "checked" : ""; ?>>ใช้งานไม่ได้</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="status" value="2" <?php echo $status == 2 ? "checked" : ""; ?>>สูญหาย</label>
                    </div>

                    <center>
                        <input type="submit" class="btn btn-primary" value="แก้ไข">
                        <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">ข้อมูลการทำรายการของอุปกรณ์</div>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ประเภทรายการ</th>
                    <th>ชื่อผู้ทำรายการ</th>
                    <th>ชื่อผู้อนุมัติ</th>
                    <th>วันที่ทำรายการ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT it.type, (SELECT u.name FROM user u WHERE u.id = it.applicant_id) applicant_name, (SELECT u.name FROM user u WHERE u.id = itd.approver_id) approver_name, itd.date FROM item_transaction_detail itd, item_transaction it WHERE itd.item_transaction_id = it.id AND itd.item_id = $item_id";

                $result = mysql_query($sql);
                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $type = $r["type"];
                    $applicant_name = $r["applicant_name"];
                    $approver_name = $r["approver_name"];
                    $date = $r["date"];

                    $type = ($type == 0 ? "เพิ่ม" : ($type == 1 ? "จำหน่าย" : ($type == 2 ? "ยืม" : "คืน")));

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>$type</td>
                            <td>$applicant_name</td>
                            <td>$approver_name</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='4'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
