<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
    $id = $_POST["id"];

    $sql = "SELECT ity.type, u.name FROM item_transaction it, user u, item_type ity WHERE it.id = $id AND it.applicant_id = u.id AND it.item_type_id = ity.id";
    $result = mysql_query($sql);
    while ($r = mysql_fetch_assoc($result)) {
        $type = $r["type"];
        $applicant_name = $r["name"];
    }
?>

<div class="panel panel-default">
    <div class="panel-heading">ข้อมูลการอนุมัติการคืนอุปกรณ์</div>
    <div class="panel-body">

        <div class="row">
            <div class="col-xs-6">
                <div class="input-group">
                    <span class="input-group-addon">เลขที่ใบขอคืน</span>
                    <input type="text" class="form-control" value="<?php echo $id; ?>" disabled>
                </div><br>
                <div class="input-group">
                    <span class="input-group-addon">ชื่อผู้ขอคืน</span>
                    <input type="text" class="form-control" value="<?php echo $applicant_name; ?>" disabled>
                </div><br>
                <div class="input-group">
                    <span class="input-group-addon">ประเภทอุปกรณ์</span>
                    <input type="text" class="form-control" value="<?php echo $type; ?>" disabled>
                </div>
            </div>
        </div>
        <br>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขทะเบียนอุปกรณ์</th>
                    <th>ชื่อผู้อนุมัติ</th>
                    <th>วันที่อนุมัติ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT i.sn, u.name, i.date FROM item_transaction_detail itd, item i, user u WHERE itd.item_transaction_id = $id AND itd.item_id = i.id AND itd.approver_id = u.id";

                $result = mysql_query($sql);
                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $sn = $r["sn"];
                    $approver_name = $r["name"];
                    $date = $r["date"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>$sn</td>
                            <td>$approver_name</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='4'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
