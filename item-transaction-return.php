<?php
    if (!(isset($c_type) && ($c_type == 2))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php

if (isset($_POST["add"])) {
    $type = $_POST["type"];
    $amount = $_POST["amount"];

    $sql = "INSERT INTO item_transaction (applicant_id, item_type_id, amount, type, date) VALUES ($c_id, $type, $amount, 3, NOW())";
    $result = mysql_query($sql);

    $sql = "SELECT type FROM item_type WHERE id = $type";
    $result = mysql_query($sql);
    while ($r = mysql_fetch_assoc($result)) {
        $type = $r["type"];
    }

    echo "
    <div class='alert alert-success alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        ขอคืนอุปกรณ์ประเภท $type จำนวน $amount เรียบร้อยแล้ว<br>โปรดนำอุปกรณ์ไปคืนเจ้าหน้าที่เพื่อลงทะเบียนการคืน
    </div>
    ";
}

?>

<div class="panel panel-default">
    <div class="panel-heading">คืนอุปกรณ์</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-offset-3 col-xs-6">
                <form method="POST" action="?page=item-transaction-return">
                    <input type="hidden" name="add">

                    <div class="input-group">
                        <span class="input-group-addon">ประเภทอุปกรณ์</span>
                        <div class="form-group">
                            <select class="form-control" name="type" required>
                                <?php
                                    $sql = "SELECT ity.id, ity.type FROM item_transaction it, item_type ity WHERE it.item_type_id = ity.id AND it.type = 2 AND it.applicant_id = $c_id
                                    AND (SELECT COUNT(*) FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.item_type_id = it.item_type_id AND itd2.item_transaction_id = it2.id AND it2.type = 2) - (SELECT COUNT(*) FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.item_type_id = it.item_type_id AND itd2.item_transaction_id = it2.id AND it2.type = 3) > 0
                                    GROUP BY ity.type";

                                    $result = mysql_query($sql);
                                    while ($r = mysql_fetch_assoc($result)) {
                                        $id = $r["id"];
                                        $type = $r["type"];

                                        echo "<option value='$id'>$type</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">จำนวนขอคืน</span>
                        <input type="number" name="amount" class="form-control" required>
                    </div><br>


                    <center>
                        <input type="submit" class="btn btn-primary" value="คืน">
                        <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">รายการอุปกรณ์ที่ต้องคืน</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-transaction-return">
            <div class="input-group">
                <input type="hidden" name="page" value="item-transaction-return">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ประเภทอุปกรณ์</th>
                    <th>จำนวนค้างคืน</th>
                    <th>จำนวนที่ต้องการคืน</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT ity.type,
                (SELECT COUNT(*) FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.item_type_id = it.item_type_id AND itd2.item_transaction_id = it2.id AND it2.type = 2) - (SELECT COUNT(*) FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.item_type_id = it.item_type_id AND itd2.item_transaction_id = it2.id AND it2.type = 3) amount_left,
                (
                    (SELECT SUM(it2.amount) FROM item_transaction it2 WHERE it2.applicant_id = $c_id AND it2.type = 3 AND it2.item_type_id = it.item_type_id) -
                    (SELECT COUNT(*) FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.item_type_id = it.item_type_id AND itd2.item_transaction_id = it2.id AND it2.type = 3)
                ) amount_want_return
                FROM item_transaction it, item_type ity
                WHERE it.applicant_id = $c_id AND it.type = 2
                AND it.item_type_id = ity.id
                AND (SELECT COUNT(*) FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.item_type_id = it.item_type_id AND itd2.item_transaction_id = it2.id AND it2.type = 2) - (SELECT COUNT(*) FROM item_transaction_detail itd2, item_transaction it2 WHERE it2.item_type_id = it.item_type_id AND itd2.item_transaction_id = it2.id AND it2.type = 3) > 0
                AND ity.type LIKE '%$search%'
                GROUP BY it.item_type_id";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $type = $r["type"];
                    $amount_left = $r["amount_left"];
                    $amount_want_return = $r["amount_want_return"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>$type</td>
                            <td>$amount_left</td>
                            <td>$amount_want_return</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='3'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของประเภทอุปกรณ์</p>
    </div>
</div>
