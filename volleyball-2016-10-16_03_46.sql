-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2016 at 08:45 PM
-- Server version: 5.7.13-log
-- PHP Version: 5.6.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `volleyball`
--
CREATE DATABASE IF NOT EXISTS `volleyball` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `volleyball`;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `item_type_id` int(11) NOT NULL,
  `sn` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `date` varchar(19) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `item_type_id`, `sn`, `status`, `date`) VALUES
(1, 1, 'ก0001', 0, '2016-10-14 05:15:17'),
(2, 1, 'ก0002', 0, '2016-10-14 05:15:17'),
(3, 2, 'ก0003', 0, '2016-10-14 05:15:17');

-- --------------------------------------------------------

--
-- Table structure for table `item_transaction`
--

DROP TABLE IF EXISTS `item_transaction`;
CREATE TABLE `item_transaction` (
  `id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `item_type_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  `date` varchar(19) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_transaction`
--

INSERT INTO `item_transaction` (`id`, `applicant_id`, `item_type_id`, `amount`, `type`, `date`) VALUES
(1, 21, 1, 2, 0, '2016-10-14 05:15:17'),
(2, 21, 2, 10, 0, '2016-10-14 05:15:17');

-- --------------------------------------------------------

--
-- Table structure for table `item_transaction_detail`
--

DROP TABLE IF EXISTS `item_transaction_detail`;
CREATE TABLE `item_transaction_detail` (
  `id` int(11) NOT NULL,
  `approver_id` int(11) NOT NULL,
  `item_transaction_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `date` varchar(19) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_transaction_detail`
--

INSERT INTO `item_transaction_detail` (`id`, `approver_id`, `item_transaction_id`, `item_id`, `date`) VALUES
(1, 20, 1, 1, '2016-10-14 05:15:17'),
(2, 20, 1, 2, '2016-10-14 05:15:17'),
(3, 20, 2, 3, '2016-10-14 05:15:17');

-- --------------------------------------------------------

--
-- Table structure for table `item_type`
--

DROP TABLE IF EXISTS `item_type`;
CREATE TABLE `item_type` (
  `id` int(11) NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_type`
--

INSERT INTO `item_type` (`id`, `type`) VALUES
(1, 'ลูกฟุตบอล'),
(2, 'ลูกวอลเลย์บอล');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `date` varchar(19) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `email`, `type`, `password`, `status`, `date`) VALUES
(21, 'bat staff', '0817371393', 'batmasterkn@gmail.com', '1', '123456', 1, '2016-10-14 04:48:19'),
(20, 'ปรเมศวร์ หอมประกอบ', '0817371393', 'batmaster_kn@hotmail.com', '0', '123456', 1, '2016-10-14 04:48:19'),
(22, 'bat student', '0817371393', 'poramate.h@ku.th', '2', '123456', 0, '2016-10-14 04:48:19'),
(23, 'staff demo', '0821213211', 'staff@s.s', '1', '123456', 1, '2016-10-14 05:15:17'),
(24, 'student a', '0817371393', 'student@s.s', '2', '123456', 1, '2016-10-16 02:09:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_transaction`
--
ALTER TABLE `item_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_transaction_detail`
--
ALTER TABLE `item_transaction_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_type`
--
ALTER TABLE `item_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `item_transaction`
--
ALTER TABLE `item_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `item_transaction_detail`
--
ALTER TABLE `item_transaction_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `item_type`
--
ALTER TABLE `item_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
