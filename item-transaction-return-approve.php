<?php
    if (!(isset($c_type) && ($c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
if (isset($_POST["approve"])) {
    $id = $_POST["id"];
    $item_id = $_POST["item_id"];

    if (!isset($id)) {
        echo "
        <div class='alert alert-danger alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            กรุณาเลือกเลขที่ใบขอคืน
        </div>
        ";
    }
    else {
        $sql = "INSERT INTO item_transaction_detail (approver_id, item_transaction_id, item_id, date) VALUES ($c_id, $id, $item_id, NOW())";
        mysql_query($sql) or die(mysql_error());

        $sql = "SELECT sn FROM item WHERE id = $item_id";
        $result = mysql_query($sql) or die(mysql_error());
        while ($r = mysql_fetch_assoc($result)) {
            $sn = $r["sn"];
        }

        echo "
        <div class='alert alert-success alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            อนุมัติการขอคืนอุปกรณ์ทะเบียน $sn เรียบร้อยแล้สว
        </div>
        ";
    }
}

?>

<div class="panel panel-default">
    <div class="panel-heading">อนุมัติการคืนอุปกรณ์</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-offset-3 col-xs-6">
                <form method="POST" action="?page=item-transaction-return-approve">
                    <input type="hidden" name="approve">

                    <div class="input-group">
                        <span class="input-group-addon">เลขที่ใบขอคืน</span>
                        <div class="form-group">
                            <select class="form-control" name="id" id="id" required>
                                <option value="-1" disabled selected>กรุณาเลือก</option>
                                <?php
                                    $sql = "SELECT it.id, ity.type FROM item_transaction it, item_type ity WHERE it.item_type_id = ity.id
                                    AND it.amount > (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id)
                                    AND it.type = 3
                                    ORDER BY it.id DESC";

                                    $result = mysql_query($sql);
                                    while ($r = mysql_fetch_assoc($result)) {
                                        $id = $r["id"];
                                        $type = $r["type"];

                                        echo "<option value='$id'>$id $type</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">เลขทะเบียนอุปกรณ์</span>
                        <div class="form-group">
                            <select class="form-control" name="item_id" id="item_id" disabled required>
                                <option value="-1" disabled selected>กรุณาเลือก</option>
                                ?>
                            </select>
                        </div>
                    </div><br>


                    <center>
                        <input type="submit" class="btn btn-primary" value="รับคืน">
                        <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">เลขที่ใบขอคืนที่ยังอนุมัติไม่ครบ</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-transaction-return-approve">
            <div class="input-group">
                <input type="hidden" name="page" value="item-transaction-return-approve">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขที่ใบขอคืน</th>
                    <th>ชื่อผู้คืน</th>
                    <th>ประเภทอุปกรณ์</th>
                    <th>จำนวนขอคืน</th>
                    <th>จำนวนอนุมัติ</th>
                    <th>วันที่ขอคืน</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT it.id,
                u.name applicant_name,
                ity.type,
                it.amount,
                (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id) amount_aprove,
                it.date
                FROM item_transaction it, item_type ity, user u
                WHERE it.item_type_id = ity.id
                AND it.type = 3
                AND it.applicant_id = u.id
                AND it.amount > (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id)
                AND (it.id LIKE '%$search%' OR u.name LIKE '%$search%' OR ity.type LIKE '%$search%' OR it.date LIKE '%$search%')
                ORDER BY it.id DESC";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $applicant_name = $r["applicant_name"];
                    $type = $r["type"];
                    $amount = $r["amount"];
                    $amount_aprove = $r["amount_aprove"];
                    $date = $r["date"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>
                                <form method='POST' action='?page=item-transaction-return-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$id</a>
                                </form>
                            </td>
                            <td>$applicant_name</td>
                            <td>$type</td>
                            <td>$amount</td>
                            <td>$amount_aprove</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='8'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของชื่อผู้ทำรายการ ประเภทอุปกรณ์<br>หรือเลขที่ใบสั่งซื้อ<br>หรือบางส่วนของวันที่สมัครในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>

<script type="text/javascript">

$('#id').on('change', function() {
    $.ajax({
        url: 'db.php',
        type: "POST",
        dataType: "json",
        data: {
            "item-transaction-return-approve": "ajax_get_available_item",
            "id": this.value
        }
    }).done(function(results) {
        console.log(results);
        $("#item_id").removeAttr("disabled");
        $("#item_id").empty();
        for (var i = 0; i < results.length; i++) {
            $("#item_id").append("<option value='" + results[i].id + "'>" + results[i].sn + "</option>");
        }
    });
});

</script>
