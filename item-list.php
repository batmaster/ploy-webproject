<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<div class="panel panel-default">
    <div class="panel-heading">ข้อมูลของอุปกรณ์</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-type">
            <div class="input-group">
                <input type="hidden" name="page" value="item-list">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขทะเบียนอุปกรณ์</th>
                    <th>ประเภทของอุปกรณ์</th>
                    <th>สถานะ</th>
                    <th>วันที่เพิ่ม</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                if ($search == "ปกติ") {
                    $status_input = "OR i.status = 0";
                }
                else if ($search == "ใช้งานไม่ได้") {
                    $status_input = "OR i.status = 1";
                }
                else if ($search == "สูญหาย") {
                    $status_input = "OR i.status = 2";
                }
                else if ($search == "ในคลัง") {
                    $status_input = "OR (SELECT it.type FROM item_transaction_detail itd, item_transaction it WHERE itd.item_id = i.id AND itd.item_transaction_id = it.id ORDER BY itd.id DESC LIMIT 1) NOT IN (1, 2)";
                }
                else if ($search == "ยืม") {
                    $status_input = "OR (SELECT it.type FROM item_transaction_detail itd, item_transaction it WHERE itd.item_id = i.id AND itd.item_transaction_id = it.id ORDER BY itd.id DESC LIMIT 1) = 2";
                }
                else if ($search == "จำหน่าย") {
                    $status_input = "OR (SELECT it.type FROM item_transaction_detail itd, item_transaction it WHERE itd.item_id = i.id AND itd.item_transaction_id = it.id ORDER BY itd.id DESC LIMIT 1) = 1";
                }

                if (isset($search)) {
                    $sql = "SELECT i.id, it.type, i.sn, i.status, i.date FROM item i, item_type it WHERE i.item_type_id = it.id
                    AND (it.type LIKE '%$search%' OR i.sn LIKE '%$search%' $status_input OR i.date LIKE '%$search%') ORDER BY i.id DESC";
                }
                else {
                    $sql = "SELECT i.id, it.type, i.sn, i.status, i.date FROM item i, item_type it WHERE i.item_type_id = it.id  ORDER BY i.id DESC";
                }
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $type = $r["type"];
                    $sn = $r["sn"];
                    $status = $r["status"];
                    $date = $r["date"];

                    $status_th = ($status == 0 ? "ปกติ" : ($status == 1 ? "ใช้งานไม่ได้" : "สูญหาย"));

                    $sql = "SELECT it.type FROM item_transaction_detail itd, item_transaction it WHERE itd.item_id = $id AND itd.item_transaction_id = it.id ORDER BY itd.id DESC LIMIT 1";
                    $result2 = mysql_query($sql);
                    while ($r2 = mysql_fetch_assoc($result2)) {
                        $type2 = $r2["type"];
                    }

                    $type2 = ($type2 == 1 ? "จำหน่าย" : ($type2 == 2 ? "ยืม" : "ในคลัง"));
                    $status_th .= " [$type2]";

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>
                                <form method='POST' action='?page=item-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$sn</a>
                                </form>
                            </td>
                            <td>$type</td>
                            <td>$status_th</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='6'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของเลขทะเบียนอุปกรณ์ ประเภทของอุปกรณ์<br>หรือสถานะ ปกติ ใช้งานไม่ได้ สูญหาญ ในคลัง ยืม จำหน่าย<br>หรือบางส่วนของวันที่สมัครในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>
