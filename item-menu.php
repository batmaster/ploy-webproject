
<?php
    if ($c_type == 0 || $c_type == 1) {
        $active = in_array($_GET['page'], array('item-type')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-type'>ข้อมูลประเภทของอุปกรณ์</a></li>";

        $active = in_array($_GET['page'], array('item-list', 'item-detail')) ? "active" : "";
        echo "<li><a class='list-group-item $active' href='?page=item-list'>ข้อมูลของอุปกรณ์</a></li>";
    }
?>
