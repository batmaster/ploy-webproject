
<ul class="nav navbar-nav">
    <?php
        if ($c_type == 0 || $c_type == 1) {
            $active = in_array($_GET['page'], array('user-register', 'user-approve', 'user-staff', 'user-student', 'user-detail')) ? "active" : "";
            echo "<li class='$active'><a href='?page=user-detail'>ข้อมูลสมาชิก</a></li>";

            $active = in_array($_GET['page'], array('item-type', 'item-list', 'item-detail')) ? "active" : "";
            echo "<li class='$active'><a href='?page=item-list'>ข้อมูลอุปกรณ์</a></li>";

            $active = in_array($_GET['page'], array('item-edit-add', 'item-edit-add-detail', 'item-edit-add-approve', 'item-edit-remove', 'item-edit-remove-detail', 'item-edit-remove-approve')) ? "active" : "";
            echo "<li class='$active'><a href='?page=item-edit-add'>การเพิ่ม/จำหน่ายอุปกรณ์</a></li>";

            $active = in_array($_GET['page'], array('item-transaction-borrow', 'item-transaction-borrow-detail', 'item-transaction-borrow-approve', 'item-transaction-borrow-report', 'item-transaction-return', 'item-transaction-return-detail', 'item-transaction-return-approve', 'item-transaction-return-report')) ? "active" : "";
            echo "<li class='$active'><a href='?page=item-transaction-borrow-report'>การยืม/คืนอุปกรณ์</a></li>";
        }
        else if ($c_type == 2) {
            $active = in_array($_GET['page'], array('user-register', 'user-approve', 'user-staff', 'user-student', 'user-detail')) ? "active" : "";
            echo "<li class='$active'><a href='?page=user-detail'>ข้อมูลสมาชิก</a></li>";

            $active = in_array($_GET['page'], array('item-transaction-borrow', 'item-transaction-borrow-approve', 'item-transaction-borrow-report', 'item-transaction-return', 'item-transaction-return-approve', 'item-transaction-return-report')) ? "active" : "";
            echo "<li class='$active'><a href='?page=item-transaction-borrow'>การยืม/คืนอุปกรณ์</a></li>";
        }
    ?>
</ul>
