<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
if (isset($_POST["approve"])) {
    $id = $_POST["id"];
    $email = $_POST["email"];

    $sql = "UPDATE user SET status = 1 WHERE id = $id";
    $result = mysql_query($sql);

    echo "
    <div class='alert alert-success alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        เปิดการใช้งาน $email เรียบร้อยแล้ว
    </div>
    ";
}

?>

<div class="panel panel-default">
    <div class="panel-heading">อนุมัติการสมัครสมาชิก</div>
    <div class="panel-body">
        <form method="GET" action="?page=user-approve">
            <div class="input-group">
                <input type="hidden" name="page" value="user-approve">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อผู้ใช้</th>
                    <th>เบอร์โทร</th>
                    <th>อีเมล์</th>
                    <th>ตำแหน่ง</th>
                    <th>วันที่สมัคร</th>
                    <th>อนุมัติ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];
                $type = ($_GET["search"] == "ผู้อำนวยการ" ? "(0)" : ($_GET["search"] == "เจ้าหน้าที่" ? "(1)" : ($_GET["search"] == "นักเรียน" ? "(2)" : "(0, 1, 2)")));

                $sql = "SELECT * FROM user WHERE status = 0 AND (name LIKE '%$search%' OR phone LIKE '%$search%' OR email LIKE '%$search%' OR type IN $type OR date LIKE '%$search%') ORDER BY id DESC";
                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $name = $r["name"];
                    $phone = $r["phone"];
                    $email = $r["email"];
                    $type = $r["type"];
                    $date = $r["date"];

                    $type = ($type == 0 ? "ผู้อำนวยการ" : ($type == 1 ? "เจ้าหน้าที่" : "นักเรียน"));

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>$name</td>
                            <td>$phone</td>
                            <td>
                                <form method='POST' action='?page=user-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <input type='hidden' name='forpage' value='user-approve'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$email</a>
                                </form>
                            </td>
                            <td>$type</td>
                            <td>$date</td>
                            <td title='อนุมัติ $email'>
                                <form method='POST' action='?page=user-approve'>
                                <input type='hidden' name='approve'>
                                <input type='hidden' name='id' value='$id'>
                                <input type='hidden' name='email' value='$email'>
                                <button type='submit' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>
                                </form>
                            </td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='7'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของชื่อผู้ใช้ เบอร์โทร อีเมล หรือตำแหน่ง<br>หรือบางส่วนของวันที่สมัครในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>
