<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
if (isset($_POST["add"])) {
    $type = $_POST["type"];
    $amount = $_POST["amount"];

    $sql = "INSERT INTO item_transaction (applicant_id, item_type_id, amount, type, date) VALUES ($c_id, $type, $amount, 0, NOW())";
    $result = mysql_query($sql);


    $sql = "SELECT type FROM item_type WHERE id = $type";

    $result = mysql_query($sql);
    while ($r = mysql_fetch_assoc($result)) {
        $type = $r["type"];
    }

    echo "
    <div class='alert alert-success alert-dismissible' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
        ขอเพิ่มอุปกรณ์ประเภท $type จำนวน $amount เรียบร้อยแล้ว
    </div>
    ";
}

?>

<?php
    if ($c_type == 1) {
        echo "
        <div class='panel panel-default'>
            <div class='panel-heading'>เพิ่มอุปกรณ์</div>
            <div class='panel-body'>
                <div class='row'>
                    <div class='col-xs-offset-3 col-xs-6'>
                        <form method='POST' action='?page=item-edit-add'>
                            <input type='hidden' name='add'>

                            <div class='input-group'>
                                <span class='input-group-addon'>ประเภทอุปกรณ์</span>
                                <div class='form-group'>
                                    <select class='form-control' name='type' required>";

                                            $sql = 'SELECT * FROM item_type it';
                                            $result = mysql_query($sql);
                                            while ($r = mysql_fetch_assoc($result)) {
                                                $id = $r['id'];
                                                $type = $r['type'];

                                                echo "<option value='$id'>$type</option>";
                                            }

                                        echo "
                                    </select>
                                </div>
                            </div><br>

                            <div class='input-group'>
                                <span class='input-group-addon'>จำนวนขอเพิ่ม</span>
                                <input type='number' name='amount' class='form-control' required>
                            </div><br>


                            <center>
                                <input type='submit' class='btn btn-primary' value='เพิ่มประเภท'>
                                <input type='dismiss' class='btn btn-danger' value='ยกเลิก'>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        ";
    }
?>

<div class="panel panel-default">
    <div class="panel-heading">ข้อมูลการเพิ่มอุปกรณ์</div>
    <div class="panel-body">
        <form method="GET" action="?page=item-edit-add">
            <div class="input-group">
                <input type="hidden" name="page" value="item-edit-add">
                <input type="text" name="search" class="form-control" placeholder="ค้นหา" value="<?php echo $_GET["search"]; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">ค้นหา</button>
                </span>
            </div>
        </form>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>เลขที่ใบสั่งซื้อ</th>
                    <th>ชื่อผู้เพิ่ม</th>
                    <th>ประเภทอุปกรณ์</th>
                    <th>จำนวนขอเพิ่ม</th>
                    <th>จำนวนอนุมัติ</th>
                    <th>วันที่ขอเพิ่ม</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $search = $_GET["search"];

                $sql = "SELECT it.id, (SELECT u.name FROM user u WHERE it.applicant_id = u.id) applicant_name,
                ity.type, it.amount,
                (SELECT COUNT(*) FROM item_transaction_detail itd WHERE itd.item_transaction_id = it.id) amount_aprove,
                it.date
                FROM item_transaction it, item_type ity
                WHERE it.type = 0
                AND it.item_type_id = ity.id
                AND ((SELECT u.name FROM user u WHERE it.applicant_id = u.id) LIKE '%$search%' OR ity.type LIKE '%$search%' OR it.id LIKE '%$search%' OR it.date LIKE '%$search%')
                ORDER BY it.id DESC";

                $result = mysql_query($sql);

                $number = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $number++;
                    $id = $r["id"];
                    $applicant_name = $r["applicant_name"];
                    $type = $r["type"];
                    $amount = $r["amount"];
                    $amount_aprove = $r["amount_aprove"];
                    $date = $r["date"];

                    echo "
                        <tr>
                            <th>$number</th>
                            <td>
                                <form method='POST' action='?page=item-edit-add-detail'>
                                    <input type='hidden' name='id' value='$id'>
                                    <input type='hidden' name='forpage' value='item-edit-add-detail'>
                                    <a href='javascript:;' onclick=\"$(this).closest('form').submit();\">$id</a>
                                </form>
                            </td>
                            <td>$applicant_name</td>
                            <td>$type</td>
                            <td>$amount</td>
                            <td>$amount_aprove</td>
                            <td>$date</td>
                        <tr>
                    ";
                }

                if ($number == 0) {
                    echo "
                        <tr>
                            <td colspan='8'>ไม่มีรายการ</td>
                        <tr>
                    ";
                }
                ?>
            </tbody>
        </table>
        <p>ค้าหาโดยข้อความบางส่วนของชื่อผู้เพิ่มอุปกรณ์ ประเภทอุปกรณ์<br>หรือเลขที่ใบขอซื้อ<br>หรือบางส่วนของวันที่ขอเพิ่มในรูปแบบ yyyy-mm-dd HH:ii:ss</p>
    </div>
</div>
