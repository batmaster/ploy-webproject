<?php
    if (!(isset($c_type) && ($c_type == 0 || $c_type == 1))) {
        header("Location: ?page=home");
        die();
    }
?>

<?php
if (isset($_POST["register"])) {
    $name = $_POST["name"];
    $phone = $_POST["phone"];
    $type = $_POST["type"];
    $email = $_POST["email"];
    $password = $_POST["password"];

    $sql = "SELECT * FROM user WHERE email = '$email' AND email = '$email'";
    $result = mysql_query($sql);

    $valid = true;
    while ($r = mysql_fetch_assoc($result)) {
        $email = $r["email"];
        $valid = false;
    }

    if ($valid) {
        $sql = "INSERT user (name, phone, type, email, password, status, date) VALUES ('$name', '$phone', '$type', '$email', '$password', 0, NOW())";
        mysql_query($sql) or die(mysql_error());
        echo "
        <div class='alert alert-success alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            กรุณาเปิดการใช้งาน $email
        </div>
        ";
    }
    else {
        echo "
        <div class='alert alert-danger alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            อีเมล์ $email ถูกใช้งานแล้ว
        </div>
        ";
    }
}

?>
<div class="panel panel-default">
    <div class="panel-heading">แก้ไขข้อมูลผู้ใช้</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-offset-3 col-xs-6">
                <form method="POST" action="?page=user-register">
                    <input type="hidden" name="register">
                    <div class="input-group">
                        <span class="input-group-addon">ชื่อผู้ใช้</span>
                        <input type="text" name="name" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon">เบอร์โทร</span>
                        <input type="text" name="phone" maxlength="10" class="form-control" pattern="[0-9]{10}" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'หมายเลขโทรศัพท์ 10 หลัก' : '');" required>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">ตำแหน่ง</span>
                        <div class="form-group">
                            <select class="form-control" name="type" required>
                                <option value="0">ผู้อำนวยการ</option>
                                <option value="1">เจ้าหน้าที่</option>
                                <option value="2">นักเรียน</option>
                            </select>
                        </div>
                    </div><br>

                    <div class="input-group">
                        <span class="input-group-addon">อีเมล์</span>
                        <input type="email" name="email" class="form-control" required>
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon">รหัสผ่าน</span>
                        <input type="text" name="password" class="form-control" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'อย่างน้อย 6 ตัวอักษร' : ''); if(this.checkValidity()) form.password2.pattern = this.value;" required>
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon">ยืนยันรหัสผ่าน</span>
                        <input type="password" name="password2" class="form-control" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'ยืนยันรหัสไม่ถูกต้อง' : '');" required>
                    </div><br>



                    <center>
                        <input type="submit" class="btn btn-primary" value="เพิ่มผู้ใช้">
                        <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>
