<?php ob_start(); ?>

<?php
require 'db_config.php';

if (isset($_POST["logout"])) {
    setcookie("id", "", time() - 3600);
    setcookie("name", "", time() - 3600);
    setcookie("email", "", time() - 3600);
    setcookie("type", "", time() - 3600);

    header("Location: ?page=home");
    die();
}
?>


<?
$c_id = $_COOKIE["id"];
$c_name = $_COOKIE["name"];
$c_email = $_COOKIE["email"];
$c_type = $_COOKIE["type"];

if (isset($c_id)) {
    setcookie("id", $c_id, time() + 3600);
    setcookie("name", $c_name, time() + 3600);
    setcookie("email", $c_email, time() + 3600);
    setcookie("type", $c_type, time() + 3600);
}
?>


<html>
<head>
    <script src="js/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/collapse.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/transition.js"></script>
    <script src="js/bootstrap-datepicker.th.min.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>

    <script src="js/loader.js"></script>

    <del><meta name="viewport" content="width=device-width, initial-scale=1"></del>
    <style>
        .container {
            width: 1150px !important;
        }
    </style>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>ระบบบริหารจัดการวัสดุ โรงเรียนบ้านเขาตาว</title>
</head>

<body>
    <div class="container">
        <img src = "images\Untitled-1.jpg" width="100%" border="0">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="?page=home"><?php if (isset($c_email)) echo "ยินดีต้อนรับ " .($c_type == 0 ? "ผู้อำนวยการ " : ($c_type == 1 ? "เจ้าหน้าที่ " : "นักเรียน ")) .$c_name; ?></a>
                    </div>

                    <div class="navbar-collapse">
                        <?php
                            if (isset($c_email)) {
                                include "top-menu.php";
                            }
                        ?>
                        <form method="POST" action="?page=home" id="form-logout">
                            <input type="hidden" name="logout">
                            <ul class="nav navbar-nav navbar-right">
                                <?php if (isset($c_email)) echo "<li><a href=\"#\" onclick=\"$(this).closest('form').submit();\">ออกจากระบบ</a></li>"; ?>
                            </ul>
                        </form>
                    </div>
                </nav>


                <div class="row">
                    <div class="col-xs-3" id="left-menu">
                        <div class="panel panel-default">
                            <ul class="nav nav-pills nav-stacked list-group">
                                <?php
                                if (!isset($c_email)) {
                                    // เมนูซ้ายมือ เมื่อยังไม่ได้ login
                                    $active = $_GET['page'] == 'home' ? 'active' : '';
                                    echo "<li><a class='list-group-item $active' href='?page=home'>หน้าแรก</a></li>";
                                    $active = $_GET['page'] == 'register' ? 'active' : '';
                                    echo "<li><a class='list-group-item $active' href='?page=register'>สมัครสมาชิก</a></li>";
                                    $active = $_GET['page'] == 'login' ? 'active' : '';
                                    echo "<li><a class='list-group-item $active' href='?page=login'>เข้าสู่ระบบ</a></li>";
                                }
                                else {
                                    // เมนูซ้ายมือ เมื่อ login แล้ว
                                    if (substr($_GET["page"], 0, 5) == "user-") {
                                        include "user-menu.php";
                                    }
                                    else if (substr($_GET["page"], 0, 6) == "item-e") {
                                        include "item-edit-menu.php";
                                    }
                                    else if (substr($_GET["page"], 0, 7) == "item-tr") {
                                        include "item-transaction-menu.php";
                                    }
                                    else if (substr($_GET["page"], 0, 5) == "item-") {
                                        include "item-menu.php";
                                    }

                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-9" id="content">
                        <?php
                        if ($_GET["page"]  && file_exists($_GET["page"] .".php")) {
                            include $_GET["page"] .".php";
                        }
                        // else {
                        //     header("Location: ?page=home");
                        // }
                        ?>
                    </div>
                </div>

                <?php
                    if ($_GET["page"] == "home" && isset($c_email)) {
                        echo "
                        <script type='text/javascript'>
                            $('#left-menu').remove();
                            $('#content').removeClass('col-xs-9');
                            $('#content').addClass('col-xs-12');
                        </script>
                        ";
                    }
                ?>


            </div>
        </body>
        </html>
        <?php
        ob_end_flush();
        ?>
