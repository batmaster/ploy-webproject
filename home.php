<div class="jumbotron">
  <h1>ยินดีต้อนรับ</h1>

  <div class="row">
      <div class="col-xs-6">
          <div id="all-item" style="width: 450px; height: 300px;"></div>
      </div>

      <div class="col-xs-6">
          <div id="available-borrowed" style="width: 450px; height: 300px;"></div>
      </div>
  </div>

</div>

<script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        new google.visualization.PieChart(document.getElementById('all-item')).draw(
            google.visualization.arrayToDataTable([
                ['อุปกรณ์', 'จำนวน'],
                <?php
                $sql = "SELECT ity.type, (SELECT COUNT(*) FROM item i2 WHERE i2.item_type_id = ity.id) amount FROM item_type ity";

                $result = mysql_query($sql) or die(mysql_error());
                while($r = mysql_fetch_assoc($result)) {
                    $type = $r["type"];
                    $amount = $r["amount"];

                    echo "['$type', $amount],";
                }
                ?>
            ])
            , {title: 'จำนวนอุปกรณ์ในระบบ'}
        );

    }
</script>
