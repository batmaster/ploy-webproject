<?php

if (isset($_POST["login"])) {
    $email = $_POST["email"];
    $password = $_POST["password"];
    $sql = "SELECT * FROM user WHERE email = '$email' AND password = '$password'";
    $result = mysql_query($sql);

    $pass = 0;
    while ($r = mysql_fetch_assoc($result)) {
        $id = $r["id"];
        $name = $r["name"];
        $email = $r["email"];
        $type = $r["type"];
        $status = $r["status"];

        if ($status == 0) {
            $pass = 1;
        }
        else {
            $pass = 2;
            setcookie("id", $id, time() + 3600);
            setcookie("name", $name, time() + 3600);
            setcookie("email", $email, time() + 3600);
            setcookie("type", $type, time() + 3600);

            header("Location: ?page=home");
            die();
        }
    }
    if ($pass == 0) {
        echo "
        <div class='alert alert-danger alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            อีเมล์หรือรหัสผ่านไม่ถูกต้อง
        </div>
        ";
    }
    else if ($pass == 1) {
        echo "
        <div class='alert alert-warning alert-dismissible' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            $email ยังไม่เปิดใช้งาน
        </div>
        ";
    }
}

?>
<br>
<div class="row">
    <div class="col-xs-offset-3 col-xs-6">
        <form method="POST" action="?page=login">
            <input type="hidden" name="login">
            <div class="input-group">
                <span class="input-group-addon">อีเมล์</span>
                <input type="email" name="email" class="form-control" required>
            </div><br>
            <div class="input-group">
                <span class="input-group-addon">รหัสผ่าน</span>
                <input type="password" name="password" class="form-control" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'อย่างน้อย 6 ตัวอักษร' : ''); if(this.checkValidity()) form.password2.pattern = this.value;" required>
            </div><br>

            <center>
                <input type="submit" class="btn btn-primary" value="เข้าสู่ระบบ">
                <input type="dismiss" class="btn btn-danger" value="ยกเลิก">
            </center>
        </form>
    </div>
</div>
